def call(body) {

    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "Получаем тело профиля по ID"
    def bodyHTML=new TestLoad(script:this,
            influxHost:config.influxHost,
            influxPort:config.influxPort,
            influxLogin:config.influxLogin,
            influxPassword:config.influxPassword,
            confluenceCredentials:config.confluenceCredentials,
            confluenceBasePageId:config.confluenceBasePageId,
            confluenceHost:config.confluenceHost,
            jmeterBaseImage:config.jmeterBaseImage,
            dockerAddress:config.dockerAddress,
            loaderId:config.loaderId,
            gitToken:config.gitToken).confluenceGetProfileBodyById(config.searchedProfileId);
    echo "Найден успешно"
    echo "Разбираем тело профиля"
    def bodyJSON=new TestLoad(script:this,
            influxHost:config.influxHost,
            influxPort:config.influxPort,
            influxLogin:config.Login,
            influxPassword:config.Password,
            confluenceCredentials:config.confluenceCredentials,
            confluenceBasePageId:config.confluenceBasePageId,
            confluenceHost:config.confluenceHost,
            jmeterBaseImage:config.jmeterBaseImage,
            dockerAddress:config.dockerAddress,
            loaderId:config.loaderId,
            gitToken:config.gitToken).confluenceParseProfileBody(bodyHTML);
    echo "Разобрано успешно"

    currentBuild.result = 'SUCCESS' //FAILURE to fail
    return bodyJSON;
}