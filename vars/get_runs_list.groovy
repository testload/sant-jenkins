def call(body) {

    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "Получаем список результатов для "+env.ProfileName

    def list=new TestLoad(script:this,
            influxHost:config.influxHost,
            influxPort:config.influxPort,
            influxLogin:config.influxLogin,
            influxPassword:config.influxPassword,
            confluenceCredentials:config.confluenceCredentials,
            confluenceBasePageId:config.confluenceBasePageId,
            confluenceHost:config.confluenceHost,
            jmeterBaseImage:config.jmeterBaseImage,
            dockerAddress:config.dockerAddress,
            loaderId:config.loaderId,
            gitToken:config.gitToken).influxDBGetRunsListByProfileName(env.ProfileName)
    echo "Найдены успешно"
    currentBuild.result = 'SUCCESS' //FAILURE to fail
    def runsList='';
    list.each{
        runsList=runsList+it.toString()+'\n';
    }

    return runsList;
}