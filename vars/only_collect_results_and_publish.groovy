def call(body) {

    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    childName="Collected_"+env.RunId;

    echo "Получаем статистику"
    def resultJSONBody=new TestLoad(script:this,
            influxHost:config.influxHost,
            influxPort:config.influxPort,
            influxLogin:config.influxLogin,
            influxPassword:config.influxPassword,
            confluenceCredentials:config.confluenceCredentials,
            confluenceBasePageId:config.confluenceBasePageId,
            confluenceHost:config.confluenceHost,
            jmeterBaseImage:config.jmeterBaseImage,
            dockerAddress:config.dockerAddress,
            loaderId:config.loaderId,
            gitToken:config.gitToken).influxDBCollectResults(env.ProfileName,env.RunId,config.usedProfileJSON);
    echo "Получена успешно"
    echo "Собираем тело дочерней страницы"
    def childHTMLBody=new TestLoad(script:this,
            influxHost:config.influxHost,
            influxPort:config.influxPort,
            influxLogin:config.influxLogin,
            influxPassword:config.influxPassword,
            confluenceCredentials:config.confluenceCredentials,
            confluenceBasePageId:config.confluenceBasePageId,
            confluenceHost:config.confluenceHost,
            jmeterBaseImage:config.jmeterBaseImage,
            dockerAddress:config.dockerAddress,
            loaderId:config.loaderId,
            gitToken:config.gitToken).confluenceBuildChildPageBody(resultJSONBody,env.Commentary)
    echo "Создано успешно"
    echo "Публикуем дочернюю страницу"
    def resultCode=new TestLoad(script:this,
            influxHost:config.influxHost,
            influxPort:config.influxPort,
            influxLogin:config.influxLogin,
            influxPassword:config.influxPassword,
            confluenceCredentials:config.confluenceCredentials,
            confluenceBasePageId:config.confluenceBasePageId,
            confluenceHost:config.confluenceHost,
            jmeterBaseImage:config.jmeterBaseImage,
            dockerAddress:config.dockerAddress,
            loaderId:config.loaderId,
            gitToken:config.gitToken,
            confluenceSpace: config.confluenceSpace).confluencePublishChildPage(config.usedProfileId,childName, childHTMLBody)
    echo "Создано успешно"



    if (resultCode.toString().equals("200")) currentBuild.result = 'SUCCESS' else currentBuild.result = 'FAILURE' //FAILURE to fail
    return resultCode;
}