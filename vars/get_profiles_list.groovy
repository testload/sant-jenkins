def call(body) {

    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "Получаем список профилей"
    def list=new TestLoad(script:this,
            influxHost:config.influxHost,
            influxPort:config.influxPort,
            influxLogin:config.influxLogin,
            influxPassword:config.influxPassword,
            confluenceCredentials:config.confluenceCredentials,
            confluenceBasePageId:config.confluenceBasePageId,
            confluenceHost:config.confluenceHost,
            jmeterBaseImage:config.jmeterBaseImage,
            dockerAddress:config.dockerAddress,
            loaderId:config.loaderId,
            gitToken:config.gitToken).confluenceGetProfilesList()

    echo "Найдены успешно"
    currentBuild.result = 'SUCCESS' //FAILURE to fail
    def profilesList='';
    list.each{
        profilesList=profilesList+it.toString()+'\n';
    }

    return profilesList;
}